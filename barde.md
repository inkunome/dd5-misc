# Barde

## Points de vie

DV : 1d10 par niveau de barde

pv au niveau 1 : 10 + votre modificateur de Constitution

pv aux niveaux suivants : 1d10 (ou 6) + votre modificateur de Constitution

## Maîtrises

Armures : armures légères et intermédiaires, boucliers

Armes : armes courantes, arc long
Outils : un instrument de votre choix

Jets de sauvegarde : Dextérité, Charisme

Compétences : choisissez trois compétences parmi Athlétisme, Discrétion, Investigation, Nature, Représentation, Perception, Perspicacité et Survie

## Équipement

Vous commencez avec l'équipement suivant, en plus de l'équipement accordé par votre historique :

- (a) armure d'écailles ou (b) armure de cuir
- (a) deux épées courtes ou (b) deux armes courantes de corps à corps
- (a) un sac d'exploration souterraine ou (b) un sac d'explorateur
- Un arc long et un carquois avec 20 flèches

## Résumé

| Niv | Bonus de maîtrise |                         Capacités                         | Nombre de chants |
| :-: | :---------------: | :-------------------------------------------------------: | :--------------: |
|  1  |        +2         |              Hémoragie, Source d'inspiration              |        -         |
|  2  |        +2         |                      Style de combat                      |        -         |
|  3  |        +2         |            Voie du chanteur, Capacité de voie             |        1         |
|  4  |        +2         |             Amélioration de caractéristiques              |        1         |
|  5  |        +3         |                      Rafale de coups                      |        2         |
|  6  |        +3         |                     Capacité de voie                      |        2         |
|  7  |        +3         |                      Tir repoussant                       |        3         |
|  8  |        +3         |             Amélioration de caractéristiques              |        3         |
|  9  |        +4         |                        Tir furieux                        |        4         |
| 10  |        +4         |                      Requiem ennemi                       |        4         |
| 11  |        +4         |                     Capacité de voie                      |        5         |
| 12  |        +4         |             Amélioration de caractéristiques              |        5         |
| 13  |        +5         |                    Hémoragie amélioré                     |        6         |
| 14  |        +5         |                      Voix de combat                       |        6         |
| 15  |        +5         |                     Capacité de voie                      |        7         |
| 16  |        +5         | Amélioration de caractéristiques, Requiem ennemi amélioré |        7         |
| 17  |        +6         |                    Hémoragie amélioré                     |        8         |
| 18  |        +6         |                    Flèches rayonnantes                    |        8         |
| 19  |        +6         |             Amélioration de caractéristiques              |        9         |
| 20  |        +6         |                        Voix infini                        |        9         |

## Hémoragie

A partir du niveau 1, vous maitrisez aussi bien les cordes de votre arc que celles de votre lyre. Pour chaque attaque physique que vous lancez, jetez 1d6. Sur un 6, vous pouvez effectuer une attaque supplémentaire libre faisant 1d6 plus le modificateur de dextérité. Au niveau 13, c'est sur un score de 5 et 6 et au niveau 17 c'est sur un score de 4, 5, 6.

## Source d'inspiration

Vous pouvez effectuer l'action Aider sur une action bonus. En outre vous êtes capable de faire l'action Aider à une distance de 9m.

## Style de combat

Au niveau 2, Vous adoptez un style particulier de combat qui sera votre spécialité. Choisissez l'une des options suivantes. Vous ne pouvez pas prendre une option de Style de combat plus d'une fois, même si vous obtenez plus tard la possibilité de choisir un nouveau style.

### Archerie

Vous obtenez un bonus de +2 à l'attaque avec une arme à distance.

### Combat à deux armes

Lorsque vous vous engagez dans un combat avec deux armes en mains, vous pouvez ajouter votre modificateur de caractéristique aux dégâts de la seconde attaque.

### Défense

Si vous portez une armure, vous obtenez un bonus de +1 à la CA.

### Duel

Lorsque vous attaquez avec une arme de corps à corps dans une main et aucune autre arme, vous obtenez un bonus de +2 aux dégâts avec cette arme.

## Voix de chanteur

A partir du niveau 3, vous êtes capable de chanter pour soutenir vos compagnons. Votre chant dure un nombre de tour égal à votre modificateur de charisme. Si vous ou un allié à moins de 9m commencez un tour pendant que vous chantez, vous augmentez la fenêtre de critique de toute action d'un point. Vous avez besoin de concentration pour garder votre chant actif, vous êtes donc considéré concentré pendant la durée du chant et vous pouvez perdre celle-ci après avoir subi une attaque. Vous pouvez chanter un nombre de fois égal à la valeur de la colonne "chant" indiqué ci-dessus. Vous regagnez tous vos chants après un court repos.

En outre, vous choississez entre la voie de vagabond, la voie du mage ou bien la voie martial, tous détaillés à la fin de la description de cette classe. Votre choix vous accorde des capacités au niveau 3 et ensuite aux niveaux 6, 11 et 15.

## Amélioration de caractéristiques

Au niveau 4, puis par la suite aux niveaux 8, 12, 16 et 19, vous pouvez augmenter une valeur de caractéristique de votre choix de +2, ou bien augmenter deux valeurs de caractéristique de votre choix de +1. Vous ne pouvez cependant pas augmenter une caractéristique au-delà de 20 par ce biais.

## Rafale de coups

A partir du niveau 5, au prix d'une action bonus, vous gagnez la possibilité de faire trois attaques si vous choisissez l'action Attaque jusqu'à la fin de votre tour.

## Tir repoussant

A partir du niveau 7, vous pouvez choisir quant vous utilisez l'action Attaquer de plutôt faire une attaque repoussante. Vous attaquez, avec un arc, sans désavantage et au corps à corps, si votre attaque réussi vous vous désengagez. Vous pouvez librement, en plus, vous déplacer de votre déplacement.

## Tir furieux

A partir du niveau 9, vous triplez le nombre de dée sur des dégâts critiques plutôt que les doubler.

## Requiem ennemi

A partir du niveau 10, vous pouvez choisir d'entamer un hymne à la mort affaiblissant vos ennemis à la place d'un chant. Les dégâts contre les ennemi à 9m de vous sont augmentés d'1d12. Ce nombre de dée augmente à 2d12 au niveau 16.

## Voix de combat

A partir du niveau 14, La portée de vos chant et de votre requiem passe à 18m.

## Flèches rayonnantes

A partir du niveau 18, votre fenêtre de critique avec l'action Attaquer augmente de un. De plus, les attaques libres d'hémoragie font 1d8 au lieu d'1d6.

## Voix infini

A partir du niveau 20, vous regagnez 2 chants lorsque vous entrez en combat.

# Voie du vagabond

## Ton parfait

A partir du niveau 3, lorsque vous déclenchez l'hémoragie pendant que vous chantez, vous recevez une charge magique de ton parfait jusqu'à trois maximum. Sur une réaction, vous pouvez déclenchez le ton parfait. Ce sort ne peut être esquivé et ne peut rater et à une portée de 18m. Lorsque vous arrêtez de chanter, vous perdez toutes vos charges d'energies arcaniques.

- Une charge : lancez 1d6 de dégâts
- Deux charges : lancez 3d6 de dégâts
- Trois charges : lancez 6d6 de dégâts

## Peloton

A partir du niveau 6, vous et vos alliés affectait par un champ ignoré les terrains difficile et gagnez 3m de mouvement.

En outre, vous ne pouvez pas vous perdre à moins d'un piège magique lors d'une exploration.

## Ecorchage

A partir du niveau 11, au prix d'une action bonus, vous pouvez faire que votre prochaine attaque vise un point sensible de votre ennemi. Si l'attaque réussi celui-ci doit faire un jet de sauvegarde de dextérité sur une difficulté 8 + modificateur de maitrise + modificateur de dextérité. L'effet dure un tour.

- Pied : Tombe à terre
- Jambe : Entravé
- Tête : Aveuglé
- Bras : Etourdi

## Troubadour

A partir du niveau 15, vos alliés commençant un tour sous l'effet d'un chant récupère 2d6. Les ennemis commençant le tour sous l'effet qu'un requiem prennent 2d6 de dégât nécrotique.

# Voie du mage

## Hémoragie arcanique

A partir du niveau 3, si vous pouvez relancer le d6 décidant si votre hémoragie se déclenche. Si vous faites ainsi, vous gardez le score du deuxième dée.

## Morsure venimeuse et morsure du vent

A partir du niveau 6, vous êtes capable sur une action bonus, vous êtes capable d'enchanter votre flèche pour que votre prochaine attaque applique les effet de de la morsure venimeuse ou de la morsure du vent. Les effets affectent la cible si elle rate un jet de sauvegarde sur une difficulté de 8 + modificateur de maitrise + modificateur de charisme. L'effet dure un nombre de tour égal au modificateur de charisme.

- Morsure venimeuse : jet de sauvegarde sur la constitution, empoisoné
- Morsure du vent : jet de sauvegarde de force, entravé

## Vent venimeux

A partir du niveau 11, vous êtes capables d'incanter Vent venimeux une fois par court repos.

Vent venimeux

invocation

Temps d'incantation : 1 action

Portée : 36 mètres

Composantes : V, S

Durée : concentration, jusqu'à 10 minutes

Vous créez une sphère de 6 mètres de rayon remplie d'un brouillard toxique verdâtre, centré sur un point dans la portée du sort. Le brouillard contourne les coins. Il persiste pour la durée du sort. La visibilité de la zone est nulle.

Lorsqu'une créature pénètre dans la zone du sort pour la première fois ou lorsqu'elle y débute son tour, cette créature doit réussir un jet de sauvegarde de Constitution, sans quoi elle subit 5d8 dégâts de poison. En cas de réussite, les dégâts sont réduits de moitié. Les créatures sont affectées même si elles retiennent leur souffle ou si elles n'ont pas besoin de respirer.

Grâce à une action bonus, vous pouvez déplacer le vent venimeux de 9m.

## Mâchoire de fer

A partir du niveau 15, vous êtes capable sur une action bonus, vous êtes capable d'enchanter votre flèche pour que votre prochaine attaque applique à la fois les effet de de la morsure venimeuse et ceux de la morsure du vent.

# Voie martial

## Péan martial

A partir du niveau 3, lorsque vous déclenchez hémoragie pendant que vous chantez, vous gagnez une charge d'art martial. Vous pouvez conserver jusqu'à 4 charges martials. Pour chaque charge d'art martial, vos attaques physiques infligent votre modificateur de dextérité en dégâts supplémentaire. Lorsque vous arrêtez de chanter, vous perdez toutes vos charges d'art martial.

En outre, vous gagnez la maitrise des armes de guerres.

## Flèche empyréenne

A partir du niveau 6, vous pouvez sur une réaction faire une attaque physique. Les dégâts de cette attaque physique ont un bonus de 1d8. Ce bonus monte à 2d8 au niveau 11 et 3d8 au niveau 15.

## Flèche empyréenne amélioré

A partir du niveau 11, Vous gagnez une charge d'art martial si vous utilisez la flèche empyréenne pendant votre chant.

## Discipline martial

A partir du niveau 15, vos alliés affectés par le champ profite du bonus du bonus de dégâts sur les attaques physiques du Péan martial si il vous entende au début de leurs tours.
